#include <stdlib.h>
#include "linkedList.h"

int nodeElementAt(struct node *firstElement, int index)
{
    int i;
    for (i = 0; i < index; i++)
    {
        firstElement = firstElement->nextElement;
    }
    return firstElement->element;
}

struct node *createNode(int element)
{
    struct node *ptr = (struct node *)malloc(sizeof(struct node));
    ptr->element = element;
    return ptr;
}

void addToNode(struct node *firstElement, int nodeSize, int element)
{
    int i;
    for (i = 0; i < nodeSize - 1; i++)
    {
        firstElement = firstElement->nextElement;
    }
    firstElement->nextElement = createNode(element);
}

void linkNodes(struct node *firstNode, struct node *lastNode)
{
    firstNode->nextElement = lastNode;
}