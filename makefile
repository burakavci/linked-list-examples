compiler=gcc
main=main
node=linkedList

main: $(main).c $(node).c
	$(compiler) $(main).c -c -o $(main).o
	$(compiler) $(node).c -c -o $(node).o
	$(compiler) $(main).o $(node).o -o $(main).exe