struct node
{
    int element;
    struct node *nextElement;
};

int nodeElementAt(struct node *firstElement, int index);
struct node *createNode(int element);
void addToNode(struct node *firstElement, int nodeSize, int element);
void linkNodes(struct node *firstNode, struct node *lastNode);